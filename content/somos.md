---
title: "Somos"
date: 2020-04-28T00:17:51-03:00
draft: false
---
 Pleno siglo XXI. Arranca este Sitio.

Sin otro impulso que las ganas, un *cuarentenazo* de calles vacías,  parroquianos en los balcones y este Escribiente con un Perro y un Gato como fieles laderos.

***La meta es el camino.***

Caben aquí estas palabras, y que el tiempo siga su curso.

>Tano
